package com.wg.shoppingcart;

/**
 * Created by Will on 14-2-8.
 */
public class ProductsConstant {

    private final static String[] ELECTRONICS = {"ipad","iphone","显示器","笔记本电脑","键盘"};
    private final static String[] FOOD = {"面包","饼干","蛋糕","牛肉","鱼","蔬菜"};
    private final static String[] COMMODITY = {"餐巾纸","收纳箱","咖啡杯","雨伞"};
    private final static String[] WINE = {"啤酒","白酒","伏特加"};

    public final static String[] TYPE = {"电子","食品","日用品","酒类"};

    static public String getProductType(String productName)
    {
        for(String type:ELECTRONICS)
        {
            if(type.equals(productName))
            {
                return TYPE[0];
            }
        }
        for(String type:FOOD)
        {
            if(type.equals(productName))
            {
                return TYPE[1];
            }
        }
        for(String type:COMMODITY)
        {
            if(type.equals(productName))
            {
                return TYPE[2];
            }
        }
        for(String type:WINE)
        {
            if(type.equals(productName))
            {
                return TYPE[3];
            }
        }
        return null;
    }
}
