package com.wg.shoppingcart;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by Administrator on 14-2-8.
 */
class Promotion {

    public Date date;
    public BigDecimal cut;
    public String type;

    public Promotion(Date date, BigDecimal cut, String type)
    {
        this.date = date;
        this.cut = cut;
        this.type = type;
    }
}
