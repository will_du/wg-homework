package com.wg.shoppingcart;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;

/**
 * Created by Administrator on 14-2-8.
 */
class ProductsList {

    private ArrayList<Product> productArrayList = new ArrayList<Product>();

    public void add(Product product)
    {
        productArrayList.add(product);
    }
    public BigDecimal getPriceByType(String typeName)
    {
        BigDecimal price = new BigDecimal(BigInteger.ZERO);
        for(Product product:productArrayList)
        {
            String productType = ProductsConstant.getProductType(product.getProductName());
            if(typeName.equals(productType))
            {
                price = price.add(product.getMultiplyPrice());
            }
        }
        return price;
    }
}
