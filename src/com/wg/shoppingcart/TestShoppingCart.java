package com.wg.shoppingcart;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by Will on 14-2-8.
 */
public class TestShoppingCart{

    @Test
    public void TestCaseA()
    {
        String input =
                "2013.11.11|0.7|电子\n"+
                "\n"+
                "1*ipad:2399.00\n"+
                "1*显示器:1799.00\n"+
                "12*啤酒:25.00\n"+
                "5*面包:9.00\n"+
                "\n"+
                "2013.11.11\n"+
                "2014.3.2 1000 200";
        String output = "3083.60";
        ShoppingCart testCase = new ShoppingCart(input);
        Assert.assertEquals(output, testCase.getPrice().toString());
    }

    @Test
    public void TestCaseB()
    {
        String input =
                "\n"+
                "\n"+
                "3*蔬菜:5.98\n"+
                "8*餐巾纸:3.20\n"+
                "\n"+
                "2014.01.01\n";
        String output = "43.54";
        ShoppingCart testCase = new ShoppingCart(input);
        Assert.assertEquals(output, testCase.getPrice().toString());
    }

    @Test
    public void TestCaseC()
    {
        String input =
                "2013.11.10|0.7|电子\n"+
                        "\n"+
                        "1*ipad:2399.00\n"+
                        "1*显示器:1799.00\n"+
                        "12*啤酒:25.00\n"+
                        "5*面包:9.00\n"+
                        "\n"+
                        "2013.11.11\n"+
                        "2014.3.2 1000 200";
        String output = "4343.00";
        ShoppingCart testCase = new ShoppingCart(input);
        Assert.assertEquals(output, testCase.getPrice().toString());
    }

    @Test
    public void TestCaseD()
    {
        String input =
                "2013.11.11|0.7|电子\n"+
                        "\n"+
                        "1*ipad:2399.00\n"+
                        "1*显示器:1799.00\n"+
                        "12*啤酒:25.00\n"+
                        "5*面包:9.00\n"+
                        "\n"+
                        "2013.11.11\n"+
                        "2013.11.11 1000 200";
        String output = "3083.60";
        ShoppingCart testCase = new ShoppingCart(input);
        Assert.assertEquals(output, testCase.getPrice().toString());
    }

    @Test
    public void TestCaseE()
    {
        String input =
                "2013.11.11|0.7|电子\n"+
                        "\n"+
                        "1*ipad:2399.00\n"+
                        "1*显示器:1799.00\n"+
                        "12*啤酒:25.00\n"+
                        "5*面包:9.00\n"+
                        "\n"+
                        "2013.11.11\n"+
                        "2013.11.10 1000 200";
        String output = "3283.60";
        ShoppingCart testCase = new ShoppingCart(input);
        Assert.assertEquals(output, testCase.getPrice().toString());
    }

    @Test
    public void TestCaseF()
    {
        String input =
                "2013.11.11|0.7|电子\n"+
                "2013.11.11|0.5|食品\n"+
                        "\n"+
                        "1*ipad:2399.00\n"+
                        "1*键盘:199.00\n"+
                        "1*显示器:1799.00\n"+
                        "12*啤酒:25.00\n"+
                        "12*白酒:54.99\n"+
                        "5*面包:9.00\n"+
                        "\n"+
                        "2013.11.11\n"+
                        "2013.11.11 2000 300";
        String output = "3760.28";
        ShoppingCart testCase = new ShoppingCart(input);
        Assert.assertEquals(output, testCase.getPrice().toString());
    }
}
