package com.wg.shoppingcart;

import java.math.BigDecimal;

/**
 * Created by Will on 14-2-8.
 */
class Product {

    private String productName;
    private BigDecimal price;
    private long count;

    public Product(long count, String productName, BigDecimal price)
    {
        this.count = count;
        this.productName = productName;
        this.price = price;
    }
    public String getProductName()
    {
        return productName;
    }
    public BigDecimal getSinglePrice()
    {
        return price;
    }
    public BigDecimal getMultiplyPrice()
    {
        return price.multiply(new BigDecimal(count));
    }
}
