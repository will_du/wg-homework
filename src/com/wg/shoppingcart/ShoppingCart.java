package com.wg.shoppingcart;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Will on 14-2-8.
 */
public class ShoppingCart {

    final private int MAX_PART = 3;//Max of count parts
    final private String TIME_FORMAT = "yyyy.MM.dd";
    private ProductsList productsList = new ProductsList();
    private PromotionList promotionList = new PromotionList();
    private Date submitDate = null;
    private ECoupon eCoupon = null;

    public ShoppingCart(String input)
    {
        String[] args0 = input.split("\n\n");

        if(args0.length != MAX_PART)
        {
            throw new IllegalArgumentException();
        }
        if(args0[0] != null && !args0[0].equals(""))
        {
            setPromotion(args0[0]);
        }
        if(args0[1] != null && !args0[1].equals(""))
        {
            setProduct(args0[1]);
        }
        String[] args1 = args0[2].split("\n");
        if(args1[0] != null && !args1[0].equals(""))
        {
            setSubmitDate(args1[0]);
        }
        if(args1.length > 1 && args1[1] != null && !args1[1].equals(""))
        {
            setECoupon(args1[1]);
        }
    }
    public BigDecimal getPrice()
    {
        BigDecimal sumPrice = new BigDecimal(BigInteger.ZERO);
        for(String type:ProductsConstant.TYPE)
        {
            BigDecimal priceByType = productsList.getPriceByType(type);
            priceByType = priceByType.multiply(promotionList.getPromotionByType(submitDate, type));
            sumPrice = sumPrice.add(priceByType);
        }
        if(eCoupon != null)
        {
            sumPrice = sumPrice.subtract(eCoupon.getMinusPrice(submitDate, sumPrice));
        }
        return sumPrice.setScale(2, BigDecimal.ROUND_HALF_UP);
    }
    private void setPromotion(String promotionString)
    {
        String[] promotions = promotionString.split("\n");
        for(String pt : promotions)
        {
            String[] info = pt.split("\\|");
            Date date = StringToDate(info[0], TIME_FORMAT);
            BigDecimal cut = new BigDecimal(new Double(info[1]));
            String type = info[2];
            promotionList.add(new Promotion(date,cut,type));
        }
    }

    /**
     * @param productString e.g.  12*啤酒:25.00
     */
    private void setProduct(String productString)
    {
        final String countPt = "*";
        final String pricePt = ":";
        String[] products = productString.split("\n");
        for(String pd : products)
        {
            long productCount = new Long(pd.substring(0,pd.indexOf(countPt)));
            String productName = pd.substring(pd.indexOf(countPt) + countPt.length(),pd.indexOf(pricePt));
            BigDecimal productPrice = new BigDecimal(pd.substring(pd.indexOf(pricePt) + pricePt.length(),pd.length()));
            productsList.add(new Product(productCount,productName,productPrice));
        }
    }
    private void setSubmitDate(String dateString)
    {
        submitDate = StringToDate(dateString, TIME_FORMAT);
    }
    private void setECoupon(String eCouponString)
    {
        final String pt = "\040";
        String[] eCoupons = eCouponString.split(pt);
        Date validDate = StringToDate(eCoupons[0], TIME_FORMAT);
        eCoupon = new ECoupon(validDate,
                    new BigDecimal(new Double(eCoupons[1])),
                    new BigDecimal(new Double(eCoupons[2])));
    }
    private static Date StringToDate(String dateStr,String formatStr){
        DateFormat dd = new SimpleDateFormat(formatStr);
        Date date = null;
        try {
            date = dd.parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

}
