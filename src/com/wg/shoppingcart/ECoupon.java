package com.wg.shoppingcart;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

/**
 * Created by Will on 14-2-8.
 */
class ECoupon {

    private Date validDate;
    private BigDecimal fullPrice;
    private BigDecimal minusPrice;

    public ECoupon(Date validDate, BigDecimal fullPrice, BigDecimal minusPrice)
    {
        this.validDate = validDate;
        this.fullPrice = fullPrice;
        this.minusPrice = minusPrice;
    }
    public BigDecimal getMinusPrice(Date submitDate, BigDecimal sumPrice)
    {
        BigDecimal retPrice = new BigDecimal(BigInteger.ZERO);
        if(fullPrice.compareTo(sumPrice) < 1 && validDate.compareTo(submitDate) > -1)
        {
            retPrice = minusPrice;
        }
        return retPrice;
    }
}
