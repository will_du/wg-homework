package com.wg.shoppingcart;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Administrator on 14-2-8.
 */
class PromotionList {

    private ArrayList<Promotion> promotionArrayList = new ArrayList<Promotion>();

    public void add(Promotion promotion)
    {
        promotionArrayList.add(promotion);
    }
    public BigDecimal getPromotionByType(Date submitDate, String typeName)
    {
        BigDecimal cut = new BigDecimal(BigInteger.ONE);
        for(Promotion promotion:promotionArrayList)
        {
            if(promotion.type.equals(typeName) && promotion.date.compareTo(submitDate) == 0)
            {
                cut = promotion.cut;
            }
        }
        return cut;
    }
}
